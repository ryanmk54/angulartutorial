var app = angular.module("app", []);
app.controller("MyController", function($scope){
  $scope.user = {
    diameter: 200,
    style:{},
  };
  $scope.calcStyle = function(user){
    user.style = {"width": user.diameter+'px', 'height' :user.diameter+'px'};
  };
  $scope.style = function(user) {
    return user.style;
  };
});

